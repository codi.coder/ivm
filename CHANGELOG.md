# Changelog

## 0.1.2 - October 07, 2020

* 7eba80c: Move --print to a subcommand

## 0.1.1 - October 07, 2020

* dde0598: Disable dynamic linking on Windows
* 867bf5f: Fix Cargo keywords
* 6e7be9b: Prepare for publishing to crates.io

## 0.1.0 - October 06, 2020

The first release of ivm.
